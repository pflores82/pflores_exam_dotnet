﻿using log4net;
using Ninject;

namespace ExamPuntoticket.Config
{
    public static class Bootstrapper
    {
        public static IKernel Kernel;
        public static void Init()
        {
            LogFactory.Configure();
            Kernel = new StandardKernel();

            Kernel.Bind<ILog>().ToMethod(x => LogManager.GetLogger("Log")).InSingletonScope();
            Kernel.Bind<CreacionArchivo>().To<CreacionArchivo>().InTransientScope();
            Kernel.Bind<EnvioEmail>().To<EnvioEmail>().InTransientScope();
            Kernel.Bind<IServiceMail>().To<ServiceMail>().InTransientScope();
            

            JobsRegister.Register();
        }
    }
}