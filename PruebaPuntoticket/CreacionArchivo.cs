﻿using System;
using System.Collections.Generic;
using System.IO;
using log4net;
using Quartz;

namespace ExamPuntoticket
{
    public class CreacionArchivo : IJob
    {
        private readonly ILog _log;

        public CreacionArchivo(ILog log)
        {
            _log = log;
        }

        public void Execute(IJobExecutionContext context)
        {
            _log.Debug("Start process file");

            var l = File.ReadAllLines(Path.Combine(Directory.GetCurrentDirectory(), "code.txt"));
            var numerated = new List<string>();
            var notNumerated = new List<string>();
            var typeGroup = new Dictionary<string, int>();

            _log.Debug("Read code types");
            foreach (var s in l)
            {
                var data = s.Split(',');
                if (data[1] == "Cancha General")
                {
                    notNumerated.Add(data[0]);
                    if (typeGroup.ContainsKey("Cancha General"))
                    {
                        typeGroup["Cancha General"] = typeGroup["Cancha General"] + 1;
                    }
                    else
                    {
                        typeGroup.Add("Cancha General", 1);
                    }
                }
                if (data[1] == "Galería")
                {
                    notNumerated.Add(data[0]);
                    if (typeGroup.ContainsKey("Galería"))
                    {
                        typeGroup["Galería"] = typeGroup["Galería"] + 1;
                    }
                    else
                    {
                        typeGroup.Add("Galería", 1);
                    }
                }
                if (data[1] == "Vip")
                {
                    numerated.Add(data[0]);
                    if (typeGroup.ContainsKey("Vip"))
                    {
                        typeGroup["Vip"] = typeGroup["Vip"] + 1;
                    }
                    else
                    {
                        typeGroup.Add("Vip", 1);
                    }
                }
                if (data[1] == "Palco")
                {
                    numerated.Add(data[0]);
                    if (typeGroup.ContainsKey("Palco"))
                    {
                        typeGroup["Palco"] = typeGroup["Palco"] + 1;
                    }
                    else
                    {
                        typeGroup.Add("Palco", 1);
                    }
                }
            }

            _log.Debug("Save file numerated");
            using (var file = new StreamWriter(Path.Combine(Directory.GetCurrentDirectory(),"numerated_"+DateTime.Now.Ticks+".txt")))
            {
                foreach (var line in numerated)
                {
                    file.WriteLine(line);
                }
            }

            _log.Debug("Save file not numerated");
            using (var file = new StreamWriter(Path.Combine(Directory.GetCurrentDirectory(), "notnumerated_" + DateTime.Now.Ticks + ".txt")))
            {
                foreach (var line in notNumerated)
                {
                    file.WriteLine(line);
                }
            }


            _log.Debug("Save file group for types");
            using (var file = new StreamWriter(Path.Combine(Directory.GetCurrentDirectory(), "typegroup_" + DateTime.Now.Ticks + ".txt")))
            {
                foreach (var line in typeGroup)
                {
                    file.WriteLine(line);
                }
            }
        }
    }
}