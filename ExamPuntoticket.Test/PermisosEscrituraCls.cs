﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;

namespace ExamPuntoticket.Test
{
    [TestClass]
    public class PermisosEscrituraCls
    {
        [TestMethod]
        public void TestEscritura()
        {
            string[] numerated = new string[] { "asd", "asd2", "asd3" };
            string path = Path.Combine(Directory.GetCurrentDirectory(), "numerated_" + DateTime.Now.Ticks + ".txt");
            using (var file = new StreamWriter(path))
            {
                foreach (var line in numerated)
                {
                    file.WriteLine(line);
                }
            }

            Assert.IsTrue(File.Exists(path));

        }
    }
}
