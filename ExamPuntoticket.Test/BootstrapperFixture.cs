﻿using ExamPuntoticket.Config;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ninject;

namespace ExamPuntoticket.Test
{
    [TestClass]
    public class BootstrapperFixture
    {
        [TestInitialize]
        public void SetUp()
        {
            Bootstrapper.Init();
        }

        [TestMethod]
        public void TestConfig()
        {
            Assert.IsInstanceOfType(Bootstrapper.Kernel.Get<IServiceMail>(), typeof(ServiceMail));
            Assert.IsInstanceOfType(Bootstrapper.Kernel.Get<CreacionArchivo>(), typeof(CreacionArchivo));
            Assert.IsInstanceOfType(Bootstrapper.Kernel.Get<EnvioEmail>(), typeof(EnvioEmail));
        }
    }
}
